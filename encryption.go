package encryption

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/sha256"
	"encoding/binary"
	"encoding/hex"
	"fmt"
)

var (
	hSourceOfIV = sha256.Sum256([]byte("sourceOfIV"))
	hCtrCipher  = sha256.Sum256([]byte("ctrCipher"))
)

func Derive(secret, purpose string) *Cipher {
	hs := sha256.Sum256([]byte(secret))
	hp := sha256.Sum256([]byte(purpose))

	hh := append(hs[:], hp[:]...)

	ivKey := sha256.Sum256(append(hh, hSourceOfIV[:]...))
	sourceOfIV, err := aes.NewCipher(ivKey[:])
	if err != nil {
		panic(err)
	}

	ctrKey := sha256.Sum256(append(hh, hCtrCipher[:]...))
	ctrCipher, err := aes.NewCipher(ctrKey[:])
	if err != nil {
		panic(err)
	}

	return &Cipher{
		sourceOfIV: sourceOfIV,
		ctrCipher:  ctrCipher,
	}
}

type Cipher struct {
	sourceOfIV cipher.Block
	ctrCipher  cipher.Block
}

func (c *Cipher) HexOf16Bytes(id string) *SubCipher {
	nonce, err := hex.DecodeString(id)
	if err != nil {
		panic(err)
	}
	if len(nonce) != 16 {
		panic(fmt.Sprintf("bad nonce length: %d", len(nonce)))
	}
	return c.fromNonce(nonce)
}

func (c *Cipher) Int(id uint64) *SubCipher {
	nonce := make([]byte, 16)
	binary.LittleEndian.PutUint64(nonce[:8], id)
	return c.fromNonce(nonce)
}

func (c *Cipher) fromNonce(nonce []byte) *SubCipher {
	iv := make([]byte, 16)
	c.sourceOfIV.Encrypt(iv, nonce)
	return &SubCipher{
		iv:        iv,
		ctrCipher: c.ctrCipher,
	}
}

type SubCipher struct {
	iv        []byte
	ctrCipher cipher.Block
}

func (c *SubCipher) XOR(offset int64, p []byte) {
	plain := make([]byte, 16)
	// Counter is xor'ed with the first half of plain.
	// Second half of plain does not change in the loop.
	for j := 8; j < 16; j++ {
		plain[j] ^= c.iv[j]
	}

	encrypted := make([]byte, 16)

	beginBlockIndex := offset / 16
	endBlockIndex := ((offset + int64(len(p))) + 15) / 16

	for i := beginBlockIndex; i < endBlockIndex; i++ {
		binary.LittleEndian.PutUint64(plain[:8], uint64(i))
		for j := 0; j < 8; j++ {
			plain[j] ^= c.iv[j]
		}
		c.ctrCipher.Encrypt(encrypted, plain)

		begin, end := 0, 16
		beginInP := i*16 - offset
		endInP := beginInP + 16

		if beginInP < 0 {
			delta := -beginInP
			begin += int(delta)
			beginInP += delta
		}
		if endInP > int64(len(p)) {
			delta := endInP - int64(len(p))
			end -= int(delta)
		}

		for j := begin; j < end; j++ {
			p[beginInP+int64(j-begin)] ^= encrypted[j]
		}
	}
}
