package encryption

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestEncryption(t *testing.T) {
	c := Derive("secret", "purpose").Int(123)

	plain := []byte("The quick brown fox jumps over the lazy dog")
	enc := make([]byte, len(plain))
	copy(enc, plain)
	c.XOR(0, enc)
	t.Logf("Texts.\nPlain: %v\nEncrp: %v", plain, enc)

	t.Run("decrypt and compare", func(t *testing.T) {
		plain2 := make([]byte, len(enc))
		copy(plain2, enc)
		c.XOR(0, plain2)
		require.Equal(t, plain, plain2)
	})

	t.Run("with offset", func(t *testing.T) {
		for offset := 1; offset <= len(enc); offset++ {
			for end := offset; end <= len(enc); end++ {
				plain2 := make([]byte, len(enc))
				copy(plain2, enc)
				plain2 = plain2[offset:end]
				c.XOR(int64(offset), plain2)
				require.Equal(t, plain[offset:end], plain2, "offset=%d", offset)
			}
		}
	})

	t.Run("wrong offset", func(t *testing.T) {
		plain2 := make([]byte, len(enc))
		copy(plain2, enc)
		c.XOR(1, plain2)
		require.NotEqual(t, plain, plain2)
		require.False(t, bytes.Contains(plain2, []byte("quick")))
	})
}

func BenchmarkEncryption(b *testing.B) {
	plain0 := []byte("The quick brown fox jumps over the lazy dog")
	var plain []byte
	for len(plain) < 1024*1024 {
		plain = append(plain, plain0...)
	}

	c := Derive("secret", "purpose").Int(123)
	enc := make([]byte, len(plain))
	copy(enc, plain)

	b.ReportAllocs()
	b.SetBytes(int64(len(enc)))
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		c.XOR(0, enc)
	}
}
